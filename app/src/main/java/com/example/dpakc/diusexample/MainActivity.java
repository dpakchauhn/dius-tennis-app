package com.example.dpakc.diusexample;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import static android.view.View.GONE;
/*
The class defines the logic for the tennis game where 1 set is played between the 2 players i.e.
Player 1 and Player 2.
The game is won by the player who wins 6 games and at least 2 games more than the opponent.
If one player has won six games and the opponent five, an additional game is played.
If the leading player wins that game, the player wins the set 7–5. If the trailing player wins
the game, a tie-break is played.
A tie break is played under different set of rules.

 */
public class MainActivity extends AppCompatActivity {

    // Declaring the required variables
    Button btnPlayer1;
    Button btnPlayer2;
    Button btnReset;
    TextView txtPlayer1;
    TextView txtPlayer2;
    TextView txtGamePlayer1;
    TextView txtGamePlayer2;
    int player1score = 0;
    int player2score = 0;
    int player1GameScore = 0;
    int player2GameScore = 0;
    boolean tiebreak = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //Initializing variable
        btnPlayer1 = (Button) findViewById(R.id.btnPlayer1);
        btnPlayer2 = (Button) findViewById(R.id.btnPlayer2);
        btnReset = (Button) findViewById(R.id.btnReset);
        txtPlayer1 = (TextView) findViewById(R.id.txtPlayer1);
        txtPlayer2 = (TextView) findViewById(R.id.txtPlayer2);
        txtGamePlayer1 = (TextView) findViewById(R.id.txtGamePlayer1);
        txtGamePlayer2 = (TextView) findViewById(R.id.txtGamePlayer2);

        btnPlayer1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Function to increment the Player 1 score
                incrementPlayer1();
            }
        });

        btnPlayer2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Function to increment the Player 2 score
                incrementPlayer2();
            }
        });

        btnReset.setOnClickListener(new View.OnClickListener() {

            // Reset the game
            @Override
            public void onClick(View v) {
                player1score = 0;
                player2score = 0;
                player1GameScore = 0;
                player2GameScore = 0;
                txtPlayer1.setText("0");
                txtPlayer2.setText("0");
                txtGamePlayer1.setText("0");
                txtGamePlayer2.setText("0");
                tiebreak = false;
                txtGamePlayer2.setVisibility(View.VISIBLE);
                txtGamePlayer1.setVisibility(View.VISIBLE);
                btnPlayer1.setEnabled(true);
                btnPlayer2.setEnabled(true);
                txtPlayer1.setVisibility(View.VISIBLE);
                txtPlayer2.setVisibility(View.VISIBLE);

            }
        });
    }

    private void incrementPlayer1() {
        pointsRule(true,false);
    }

    private void incrementPlayer2() {
        pointsRule(false,true);
    }

    private void pointsRule(boolean player1bool, boolean player2bool) {

        // Incrementing the score for the Player 1 and displaying the value on the screen
        if(player1bool) {
            if (!tiebreak) {
                switch (player1score) {
                    case 0:
                        player1score = 15;
                        txtPlayer1.setText(player1score + "");
                        break;
                    case 15:
                        player1score = 30;
                        txtPlayer1.setText(player1score + "");
                        break;

                    case 30:
                        player1score = 40;
                        txtPlayer1.setText(player1score + "");
                        if (player1score == 40 && player2score == 40) {
                            txtPlayer1.setText("Deuce");
                            txtPlayer2.setText("Deuce");
                            player1score = 41;
                            player2score = 41;

                        }
                        break;
                    case 40:
                        checkIfWon(player1bool, player2bool);
                        break;

                    default:
                        player1score = player1score + 1;
                        checkIfWon(player1bool, player2bool);
                        break;
                }
            }
            else
            {
                player1score = player1score + 1;
                txtPlayer1.setText(player1score+"");
                checkForTieWin(player1bool, player2bool);
            }
        }

        if(player2bool)

        // Incrementing the score for the Player 2 and displaying the value on the screen
        {
            if (!tiebreak) {

                switch (player2score) {
                    case 0:
                        player2score = 15;
                        txtPlayer2.setText(player2score + "");
                        break;

                    case 15:
                        player2score = 30;
                        txtPlayer2.setText(player2score + "");
                        break;

                    case 30:
                        player2score = 40;
                        txtPlayer2.setText(player2score + "");
                        if (player1score == 40 && player2score == 40) {
                            txtPlayer1.setText("Deuce");
                            txtPlayer2.setText("Deuce");
                            player1score = 41;
                            player2score = 41;

                        }
                        break;

                    case 40:
                        checkIfWon(player1bool, player2bool);
                        break;

                    default:
                        player2score = player2score + 1;
                        checkIfWon(player1bool, player2bool);
                        break;
                }
            }
            else
            {
                player2score = player2score + 1;
                txtPlayer2.setText(player2score+"");
                checkForTieWin(player1bool, player2bool);
            }
        }
    }

    private void checkForTieWin(boolean player1bool, boolean player2bool) {
        // Win condition for the game
        if((player1score >= 7 && ((player1score - player2score) >= 2)) || (player2score >= 7 && ((player2score - player1score) >= 2)) )
        {
            if(player1bool)
            {
                Toast.makeText(this,"Player 1 won the game",Toast.LENGTH_SHORT).show();

            }
            else if(player2bool)
            {
                Toast.makeText(this,"Player 2 won the game",Toast.LENGTH_SHORT).show();


            }
            incrementGame(player1bool, player2bool);

        }
    }

    private void checkIfWon(boolean player1bool, boolean player2bool) {
        // Win condition for the set for the game
        if(player1bool)
        {
            if(player2score <= 30)
            {
                Toast.makeText(this,"Player 1 won the set",Toast.LENGTH_SHORT).show();

                txtPlayer1.setText("0");
                txtPlayer2.setText("0");
                player1score = 0;
                player2score = 0;
                incrementGame(player1bool, player2bool);
            }
            else
            {

                int diff = player1score - player2score;
                if(diff == 0)
                {
                    txtPlayer1.setText("Deuce");
                    txtPlayer2.setText("Deuce");
                    player1score = 41;
                    player2score = 41;
                }
                else if(diff == 1)
                {
                    txtPlayer1.setText("Advantage");
                    txtPlayer2.setText("40");
                }
                else if(diff == 2)
                {
                    Toast.makeText(this,"Player 1 won the set",Toast.LENGTH_SHORT).show();

                    txtPlayer1.setText("0");
                    txtPlayer2.setText("0");
                    player1score = 0;
                    player2score = 0;
                    incrementGame(player1bool, player2bool);

                }

            }
        }
        else if(player2bool)
        {
            if(player1score <= 30)
            {
                Toast.makeText(this,"Player 2 won the set",Toast.LENGTH_SHORT).show();

                txtPlayer2.setText("0");
                txtPlayer1.setText("0");
                player1score = 0;
                player2score = 0;
                incrementGame(player1bool, player2bool);

            }
            else
            {

                int diff = player2score - player1score;
                if(diff == 0)
                {
                    txtPlayer1.setText("Deuce");
                    txtPlayer2.setText("Deuce");
                    player1score = 41;
                    player2score = 41;
                }
                else if(diff == 1)
                {
                    txtPlayer2.setText("Advantage");
                    txtPlayer1.setText("40");
                }
                else if(diff == 2)
                {
                    Toast.makeText(this,"Player 2 won the set",Toast.LENGTH_SHORT).show();

                    txtPlayer2.setText("0");
                    txtPlayer1.setText("0");
                    player1score = 0;
                    player2score = 0;
                    incrementGame(player1bool, player2bool);

                }

            }
        }

    }

    private void incrementGame(boolean player1bool, boolean player2bool) {
        if(player1bool)
        {
            player1GameScore = player1GameScore + 1;
            if(player1GameScore >=6 && ((player1GameScore - player2GameScore) >= 2))
            {
                txtGamePlayer1.setText("Player1 won");
                txtGamePlayer2.setVisibility(GONE);
                txtPlayer1.setVisibility(GONE);
                txtPlayer2.setVisibility(GONE);
                btnPlayer1.setEnabled(false);
                btnPlayer2.setEnabled(false);
                Toast.makeText(this,"Player 1 won the game",Toast.LENGTH_SHORT).show();
            }
            else if(player1GameScore == 6 && player2GameScore == 6)
            {
                txtGamePlayer1.setText(player1GameScore + "");

                Toast.makeText(this,"A Tie Break will be played",Toast.LENGTH_SHORT).show();
                player1score = 0;
                player2score = 0;
                tiebreak = true;
            }
            else
            {
                txtGamePlayer1.setText(player1GameScore + "");
            }
        }
        else if(player2bool)
        {
            player2GameScore = player2GameScore + 1;
            if(player2GameScore >=6 && ((player2GameScore - player1GameScore) >= 2))
            {
                txtGamePlayer2.setText("Player2 won");
                txtGamePlayer1.setVisibility(GONE);
                txtPlayer1.setVisibility(GONE);
                txtPlayer2.setVisibility(GONE);
                btnPlayer1.setEnabled(false);
                btnPlayer2.setEnabled(false);
                Toast.makeText(this,"Player 1 won the game",Toast.LENGTH_SHORT).show();
            }
            else if(player1GameScore == 6 && player2GameScore == 6)
            {
                txtGamePlayer2.setText(player2GameScore + "");

                Toast.makeText(this,"A Tie Break will be played",Toast.LENGTH_SHORT).show();
                player1score = 0;
                player2score = 0;
                tiebreak = true;
            }
            else {
                txtGamePlayer2.setText(player2GameScore + "");
            }
        }
    }
}
